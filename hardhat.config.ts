import "@nomicfoundation/hardhat-toolbox";
import { HardhatUserConfig } from "hardhat/config";
import "solidity-coverage";
import "@nomiclabs/hardhat-solhint";

import "./tasks/add-claim.task";
import "./tasks/add-key.task";
import "./tasks/deploy-identity.task";
import "./tasks/deploy-proxy.task";
import "./tasks/remove-claim.task";
import "./tasks/remove-key.task";
import "./tasks/revoke.task";

const config: HardhatUserConfig = {
  solidity: "0.8.17",
  networks: {
    mumbai: {
      url: "https://polygon-mumbai.g.alchemy.com/v2/zlp9q2zCH4WYMQKz81ju2d5AyGZw3Yxf",
      accounts: [
        "61f3ef5cb1e4ec5e2a0856339c7b73388e1925ccefa73ddfbbf02a4d13978c36",
        "ee15e9665beb3c0688b90cc91d6d236c922e81a8c95efc4380a100ab808d2686",
      ],
    },
  },
};

export default config;
