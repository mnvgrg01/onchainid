import { ethers } from "hardhat";

async function main() {
  const [platform, user] = await ethers.getSigners();

  // console.log(identityOwner);

  const walletAddress = user.address;
  console.log("wallet Address of the user: -- ", walletAddress);

  const Identity = await ethers.getContractFactory("Identity");
  const identity = await Identity.connect(platform).deploy(
    walletAddress,
    false
  );

  console.log(
    `Deploying identity for ${walletAddress} at ${identity.address} ...`
  );

  await identity.deployed();

  console.log(
    `Deployed identity for ${walletAddress} at ${identity.address} !`
  );
}

main().catch((error) => {
  console.error(error);
  process.exitCode = 1;
});
