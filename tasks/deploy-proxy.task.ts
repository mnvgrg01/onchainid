import { task } from "hardhat/config";
import { TaskArguments } from "hardhat/types";

task("deploy-proxy", "Deploy an identity as a proxy using a factory")
  .addParam("from", "Will pay the gas for the transaction")
  .addParam("factory", "The address of the identity factory")
  .addParam(
    "key",
    "The ethereum address that will own the identity (as a MANAGEMENT key)"
  )
  .addOptionalParam("salt", "A salt to use when creating the identity")
  .setAction(async (args: TaskArguments, hre) => {
    const signer = await hre.ethers.getSigner(args.from);

    const factory = await hre.ethers.getContractAt(
      "IdFactory",
      args.factory,
      signer
    );
    // const tx = await factory.createIdentity(args.key, args.salt ?? args.key);

    const keyHash = hre.ethers.utils.keccak256(
      hre.ethers.utils.defaultAbiCoder.encode(
        ["address"],
        ["0x11df38f2a2d9f9641a4b5fc442c8860e8b101561"]
      )
    );

    const tx = await factory.createIdentityWithManagementKeys(
      args.key,
      args.salt,
      [keyHash],
      { gasLimit: 3000000 }
    );

    // use this in case of adding the key createIdentityWithManagementKeys

    console.log(
      `Deploy a new identity as a proxy using factory ${factory.address} . tx: ${tx.hash}`
    );

    await tx.wait();

    const identityAddress = await factory.getIdentity(args.key);

    console.log(
      `Deployed a new identity at ${identityAddress} as a proxy using factory ${factory.address} . tx: ${tx.hash}`
    );
  });
