import { task } from "hardhat/config";
import { TaskArguments } from "hardhat/types";
// import { ethers } from "hardhat";

task("add-claim", "Add a claim to an identity")
  .addParam("identity", "The address of the identity")
  .addParam("from", "A CLAIM key on the claim issuer")
  // .addParam("claim", "The content of a claim as a JSON string")
  .setAction(async (args: TaskArguments, hre) => {
    const signer = await hre.ethers.getSigner(args.from);

    const identity = await hre.ethers.getContractAt(
      "Identity",
      args.identity,
      signer
    );

    const claimIssuer = await hre.ethers.getContractAt(
      "ClaimIssuer",
      "0x80F03c0f47273994a23bA5424379874F59727A30",
      signer
    );

    // const claim = JSON.parse(args.claim);

    const issuer = "0x80F03c0f47273994a23bA5424379874F59727A30";

    let data = { name: "Bob", gender: "male", birthDate: "01231994" }; // for issue

    const encodedData = hre.ethers.utils.sha256(
      hre.ethers.utils.toUtf8Bytes(JSON.stringify(data))
    );

    let claim = {
      identity: args.identity,
      issuer: issuer,
      topic: 10101000100001,
      scheme: 10101000666002,
      data: encodedData,
      uri: "",
      signature: "",
    };

    claim.signature = await signer.signMessage(
      hre.ethers.utils.arrayify(
        hre.ethers.utils.keccak256(
          hre.ethers.utils.defaultAbiCoder.encode(
            ["address", "uint256", "bytes"],
            [claim.identity, claim.topic, claim.data]
          )
        )
      )
    );

    console.log(claim);

    ////////////////////////////////////////////////////////////////////////////////////////////////////

    // to add the claim

    ////////////////////////////////////////////////////////////////////////////////////////////////////

    const tx = await identity
      .connect(signer)
      .addClaim(
        claim.topic,
        claim.scheme,
        claim.issuer,
        claim.signature,
        claim.data,
        claim.uri,
        { gasLimit: 3000000 }
      );

    console.log(
      `Add claim of topic ${claim.topic} on identity ${args.identity} tx: ${tx.hash}`
    );

    await tx.wait();

    console.log(
      `Add claim of topic ${claim.topic} on identity ${args.identity} tx mined: ${tx.hash}`
    );

    ////////////////////////////////////////////////////////////////////////////////////////////////////

    // Is the claim valid or not check:

    ///////////////////////////////////////////////////////////////////////////////////////////////////

    // const claimId = await identity
    //   .connect(signer)
    //   .getClaimIdsByTopic(10101000100001);

    // console.log("the claim id :--> ", claimId);

    // const Idclaim = await identity.connect(signer).getClaim(claimId[0]);

    // console.log("the claim : --> ", Idclaim);

    // const tx = await claimIssuer.connect(signer).isClaimValid(
    //   args.identity,
    //   Idclaim.topic,
    //   Idclaim.signature,
    //   Idclaim.data // here you can verify against the claim data.
    // );

    // console.log("Is the claim valid : --> ", tx);
  });
